# DWM Keymap Cheatsheet

## XF86 Keys

- **Volume Up**:                  `XF86XK_AudioRaiseVolume`
- **Volume Down**:                `XF86XK_AudioLowerVolume`
- **Mute Volume**:                `XF86XK_AudioMute`
- **Brightness Up**:              `XF86XK_MonBrightnessUp`
- **Brightness Down**:            `XF86XK_MonBrightnessDown`

## Custom Mappings

- **Browser**:                    `CTRL+ SFT + b`
- **File Manager**:               ` + n`
- **Volume Control**:           律 ` + p`
- **Emoji Selector**:           😀 ` + e`
- **Screenshot**:               📸 ` + s`
- **Kill Status Bar**:          🚫 ` + CTRL+F8`
- **System Monitor**:             ` + SFT+n`
- **Color Picker**:             🎨 ` + SFT+c`
- **Volume Mixer**:             隣 ` + SFT+e`
- **Clipmenu**:                 📋 ` + SFT+p`
- **Select Screenshot**:        🖼️ ` + SFT+s`

## Layouts

- **Grid Layout**:              ﱹ  ` + g`
- **Bstack Layout**:            粒 ` + SFT+b`
- **Centered Master**:            ` + m`
- **Monocle Layout**:           🗒️ ` + CTRL+m`
- **Spiral Layout**:            🌀 ` + CTRL+S`
- **Dwindle Layout**:           📉 ` + CTRL+D`
- **Deck Layout**:              🃏 ` + CTRL+D`
- **Bstack Horiz Layout**:      🗂️ ` + CTRL+B`
- **Horiz Grid Layout**:        📊 ` + CTRL+H`
- **Gapless Grid Layout**:      📏 ` + CTRL+G`
- **Centered Floating Master**: 🗂️ ` + CTRL+C`

## DWM Commands

- **Launch Dmenu**:               ` + d`
- **Launch Terminal**:            ` + x`
- **Toggle Bar**:                 ` + b`
- **Focus Next Window**:          ` + j`
- **Focus Previous Window**:      ` + k`
- **Increase Master Clients**:    ` + SFT+i`
- **Decrease Master Clients**:    ` + SFT+d`
- **Decrease Master Size**:       ` + h`
- **Increase Master Size**:       ` + l`
- **Move Stack Up**:            ⬆️  ` + SFT+j`
- **Move Stack Down**:          ⬇️  ` + SFT+k`
- **Zoom Window**:              🔍 ` + Return`

## Gap Adjustments

### General Gaps
- **Increase Gaps**:            `` ` + ALT+u`
- **Decrease Gaps**:            `` ` + ALT+SFT+u`
- **Toggle Gaps**:              `` ` + ALT+0`
- **Default Gaps**:             `` ` + ALT+SFT+0`

### Inner Gaps
- **Increase Inner Gaps**:      `` ` + ALT+i`
- **Decrease Inner Gaps**:      `` ` + ALT+SFT+i`

#### Horizontal Inner Gaps
- **Increase Horizontal Inner Gaps**: `` ` + ALT+6`
- **Decrease Horizontal Inner Gaps**: `` ` + ALT+SFT+6`

#### Vertical Inner Gaps
- **Increase Vertical Inner Gaps**: `` ` + ALT+7`
- **Decrease Vertical Inner Gaps**: `` ` + ALT+SFT+7`

### Outer Gaps
- **Increase Outer Gaps**:      `` ` + ALT+o`
- **Decrease Outer Gaps**:      `` ` + ALT+SFT+o`

#### Horizontal Outer Gaps
- **Increase Horizontal Outer Gaps**: `` ` + ALT+8`
- **Decrease Horizontal Outer Gaps**: `` ` + ALT+SFT+8`

#### Vertical Outer Gaps
- **Increase Vertical Outer Gaps**: `` ` + ALT+9`
- **Decrease Vertical Outer Gaps**: `` ` + ALT+SFT+9`


## Tag Management

- **Switch to Previous Tag**:   ⏪ ` + Tab`
- **Switch to Next Tag**:       ⏩ `ALT + Tab`
- **View All Tags**:            🔢 ` + 0`
- **Tag All Windows**:          🏷️ ` + SFT+0`
- **Focus Previous Monitor**:   📉 ` + comma`
- **Focus Next Monitor**:       📈 ` + period`
- **Tag Monitor**:              🖥️ ` + SFT+comma`
- **Untag Monitor**:            🖥️ ` + SFT+period`
- **Toggle Tag Display Mode**:  🔄 ` + CTRL+t`
- **Switch Tags**:              🔢 `TAGKEYS(XK_1, 0) - TAGKEYS(XK_9, 8)`

## Quit

- **Quit DWM**:                 ❌ ` + SFT+q`

## Miscellaneous

- **Toggle Colorful Tags**:     🎨 ` + F1`
