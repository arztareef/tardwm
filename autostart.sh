#!/bin/zsh
xset r rate 350 75 &
nitrogen --restore &
lxsession &
# picom &
xcompmgr &
nm-applet &
# setsid /home/tareef/.local/share/chadwm/scripts/bar.sh &
# dwmblocks &
sbar &
xss-lock slock &
pactl set-sink-volume @DEFAULT_SINK@ 100% &
clipmenud & -- Clipboard manager
