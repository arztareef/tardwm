/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 0;        /* border pixel of windows */
static const unsigned int barborderpx  = 3;        /* border pixel of bar */
static const char *occupied_symbol = "";
static const char *unoccupied_symbol = "";
static const unsigned int snap      = 2;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft  = 0;   /* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int vertpad            = 0;       /* vertical padding of bar */
static const int sidepad            = 0;       /* horizontal padding of bar */
static const int user_bh            = 26;        /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const unsigned int colorfultitle  = 1;  /* 0 means title use SchemeTitle and SchemeTitleFloat */
static const unsigned int colorfultag    = 1;  /* 0 means use SchemeSel for selected tag */
static const int horizpadbar        = 8;        /* horizontal padding for statusbar */
static const int vertpadbar         = 6;        /* vertical padding for statusbar (atleast 2) */
static const int systraysize        = 16;          /* systray icon size */
static const unsigned int gappih    = 5;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 5;       /* vert inner gap between windows */
static const unsigned int gappoh    = 5;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 5;       /* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
#define ICONSIZE 16   /* icon size */
#define ICONSPACING 8 /* space between icon and title */
static const char *fonts[]          = { 
  "JetBrains Mono:style=medium:antialias=true:autohint=true:size=10",
  "JetBrainsMono Nerd Font:style=extrabold:antialias=true:autohint=true:size=10",
  "JoyPixels:antialias=true:autohint=true:size=10",
};

#include "themes/catppuccin-mocha.h"

/* tagging */
// static const char *tags[] = { "", "", "", "", "" };
static const char *tags[] = { "", "󰮯", "", "󰇮", "" };

static const int tagschemes[] = { SchemeTag1, SchemeTag2, SchemeTag3,
                                  SchemeTag4, SchemeTag5, SchemeTag6,
                                  SchemeTag7, SchemeTag8, SchemeTag9 };


/* Underline */
static const unsigned int ulinepad	= 5;	/* horizontal padding between the underline and tag */
static const unsigned int ulinestroke	= 2;	/* thickness / height of the underline */
static const unsigned int ulinevoffset	= 0;	/* how far above the bottom of the bar the line should appear */
static const int ulineall 		= 0;	/* 1 to show underline on all tags, 0 for just the active ones */

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
  /* class                                        instance    title       tags mask     iscentered   isfloating   monitor */
  { "Gimp",                                       NULL,       NULL,       0,            1,           1,           -1 },
  { "Chromium",                                  NULL,       NULL,       0,            1,           0,           -1 },
  { "Firefox",                                    NULL,       NULL,       0,            1,           0,           -1 },
  { "Pcmanfm",                                    NULL,       NULL,       0,            1,           0,           -1 },
  { "st-256color",                                NULL,       NULL,       0,            1,           0,           -1 },
  { "Gcr-prompter",                               NULL,       NULL,       0,            1,           1,           -1 },
  { "Lxpolkit",                                   NULL,       NULL,       0,            1,           1,           -1 },
  { "Lxsession-default-apps",                     NULL,       NULL,       0,            1,           1,           -1 },
  { "nm-applet",                                  NULL,       NULL,       0,            1,           1,           -1 },
  { "Windscribe2",                                NULL,       NULL,       0,            1,           1,           -1 },
  { "Xdg-desktop-portal-gtk",                     NULL,       NULL,       0,            1,           1,           -1 },
  { "Nl.hjdskes.gcolor3",                                    NULL,       NULL,       0,            1,           1,           -1 },
  // { "Firefox",  NULL,       NULL,       1 << 8,       0,           0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int attachdirection = 3;    /* 0 default, 1 above, 2 aside, 3 below, 4 bottom, 5 top */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "󰕮",      tile },    /* first entry is default */
	{ "",      NULL },    /* no layout function means floating behavior */
	{ "|M|",      centeredmaster },
	{ "󰕯",      monocle },
	{ "󰁥",      spiral },
	{ "󰕴",     dwindle },
	{ "󱢡",      deck },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
	{ "󰕳",      grid },
	{ "###",      nrowgrid },
	{ "---",      horizgrid },
	{ ":::",      gaplessgrid },
	{ ">M>",      centeredfloatingmaster },
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *dmenucmd[] = { "dmenu_run", NULL };
static const char *termcmd[]  = { "st", NULL };

static const char volup[] = { "pactl set-sink-volume @DEFAULT_SINK@ +10%; kill -44   $(cat ~/.cache/pidofbar)" };
static const char voldn[] = { "pactl set-sink-volume @DEFAULT_SINK@ -10%; kill -44   $(cat ~/.cache/pidofbar)" };
static const char volmute[] = { "pactl set-sink-mute @DEFAULT_SINK@ toggle; kill -44 $(cat ~/.cache/pidofbar)" };

static const char briup[] = { "brightnessctl s +5%; kill -49 $(cat ~/.cache/pidofbar)" };
static const char bridn[] = { "brightnessctl s 5%-; kill -49 $(cat ~/.cache/pidofbar)" };

static const char browsercmd[] = { "firefox" };
static const char filemanager[] = { "pcmanfm" };

static const char volumemixer[] = { "st -e pulsemixer" };
static const char btopcmd[] = { "st -e btop" };
static const char pavucmd[] = { "pavucontrol" };

static const char screenshotcmd[] = { "screenshot" };
static const char selectscreenshotcmd[] = { "selectscreenshot" };

static const char colorcmd[] = { "xcolor | tr -d \'\n\' | xclip -i -selection clipboard && notify-send \"Color Copied.\" \"Color $(xclip -o -selection clipboard) copied to the clipboard.\"" };

static const char emojiselectorcmd[] = { "emojiselector" };

static const char killsbar[] = { "kill -9 $(cat ~/.cache/pidofbar) && xsetroot -name \"Killed sbar.\"" };

static const char clipmenu[] = { "clipmenu" };


#include <X11/XF86keysym.h>
#include "movestack.c"
static const Key keys[] = {
	/* modifier                     key                                   function                    argument */

  // XF86 Keys
  { 0,                            XF86XK_AudioRaiseVolume,               spawn,                       SHCMD(volup)},
  { 0,                            XF86XK_AudioLowerVolume,               spawn,                       SHCMD(voldn)},
  { 0,                            XF86XK_AudioMute,                      spawn,                       SHCMD(volmute)},

  { 0,                            XF86XK_MonBrightnessUp,                spawn,                        SHCMD(briup) },
  { 0,                            XF86XK_MonBrightnessDown,              spawn,                       SHCMD(bridn) },

  // Custom Mappings
  { ControlMask|ShiftMask,        XK_b,                                   spawn,                      SHCMD(browsercmd) },
  { MODKEY,                       XK_n,                                   spawn,                      SHCMD(filemanager) },
  { MODKEY,                       XK_p,                                   spawn,                      SHCMD(pavucmd) },
  { MODKEY,                       XK_e,                                   spawn,                      SHCMD(emojiselectorcmd) },
  { MODKEY,                       XK_s,                                   spawn,                      SHCMD(screenshotcmd) },
  { MODKEY|ControlMask,           XK_F8,                                   spawn,                      SHCMD(killsbar) },
  { MODKEY|ShiftMask,             XK_n,                                   spawn,                      SHCMD(btopcmd) },
  { MODKEY|ShiftMask,             XK_c,                                   spawn,                      SHCMD(colorcmd) },
  { MODKEY|ShiftMask,             XK_e,                                   spawn,                      SHCMD(volumemixer) },
  { MODKEY|ShiftMask,             XK_p,                                   spawn,                      SHCMD(clipmenu) },
  { MODKEY|ShiftMask,             XK_s,                                   spawn,                      SHCMD(selectscreenshotcmd) },

  // Layouts
	{ MODKEY,                       XK_r,                                 setlayout,                   {.v = &layouts[5]} },
	{ MODKEY|ShiftMask,                       XK_r,                                 setlayout,                   {.v = &layouts[4]} },
	{ MODKEY,                       XK_g,                                 setlayout,                   {.v = &layouts[12]} },
	{ MODKEY|ShiftMask,                       XK_g,                                 setlayout,                   {.v = &layouts[9]} },
	{ MODKEY|ShiftMask,                       XK_b,                                 setlayout,                   {.v = &layouts[7]} },
	{ MODKEY,                       XK_m,                                 setlayout,                   {.v = &layouts[2]} },

  // DWM Mappings
	{ MODKEY,                       XK_d,                                 spawn,                       {.v = dmenucmd } },
	{ MODKEY,                       XK_x,                                 spawn,                       {.v = termcmd } },
	{ MODKEY,                       XK_b,                                 togglebar,                   {0} },
	{ MODKEY,                       XK_j,                                 focusstack,                  {.i = +1 } },
	{ MODKEY,                       XK_k,                                 focusstack,                  {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_i,                                 incnmaster,                  {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_d,                                 incnmaster,                  {.i = -1 } },
	{ MODKEY,                       XK_h,                                 setmfact,                    {.f = -0.05} },
	{ MODKEY,                       XK_l,                                 setmfact,                    {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_j,                                 movestack,                   {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,                                 movestack,                   {.i = -1 } },
	{ MODKEY,                       XK_Return,                            zoom,                        {0} },
	{ MODKEY|Mod1Mask,              XK_u,                                 incrgaps,                    {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_u,                                 incrgaps,                    {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_i,                                 incrigaps,                   {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_i,                                 incrigaps,                   {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_o,                                 incrogaps,                   {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_o,                                 incrogaps,                   {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_6,                                 incrihgaps,                  {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_6,                                 incrihgaps,                  {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_7,                                 incrivgaps,                  {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_7,                                 incrivgaps,                  {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_8,                                 incrohgaps,                  {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_8,                                 incrohgaps,                  {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_9,                                 incrovgaps,                  {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_9,                                 incrovgaps,                  {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_0,                                 togglegaps,                  {0} },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_0,                                 defaultgaps,                 {0} },
	{ MODKEY,                       XK_Tab,                               view,                        {0} },
	{ Mod1Mask,                     XK_Tab,                               shiftviewclients,            { .i = +1 } },
	{ ShiftMask|Mod1Mask,           XK_Tab,                               shiftviewclients,            { .i = -1 } },
	{ MODKEY,                       XK_c,                                 killclient,                  {0} },
	{ MODKEY,                       XK_t,                                 setlayout,                   {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,                                 setlayout,                   {.v = &layouts[1]} },
	{ MODKEY|ShiftMask,                       XK_m,                                 setlayout,                   {.v = &layouts[2]} },
	{ MODKEY|ControlMask,		        XK_comma,                             cyclelayout,                 {.i = -1 } },
	{ MODKEY|ControlMask,           XK_period,                            cyclelayout,                 {.i = +1 } },
	{ MODKEY,                       XK_space,                             setlayout,                   {0} },
	{ MODKEY|ShiftMask,             XK_space,                             togglefloating,              {0} },
	{ MODKEY|ShiftMask,             XK_f,                                 togglefullscr,               {0} },
	{ MODKEY,                       XK_0,                                 view,                        {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,                                 tag,                         {.ui = ~0 } },
	{ MODKEY,                       XK_comma,                             focusmon,                    {.i = -1 } },
	{ MODKEY,                       XK_period,                            focusmon,                    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,                             tagmon,                      {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period,                            tagmon,                      {.i = +1 } },
  { MODKEY|ControlMask,           XK_t,                                 toggle_tag_display_mode,     {0} },
	TAGKEYS(                        XK_1,                                                              0)
	TAGKEYS(                        XK_2,                                                              1)
	TAGKEYS(                        XK_3,                                                              2)
	TAGKEYS(                        XK_4,                                                              3)
	TAGKEYS(                        XK_5,                                                              4)
	TAGKEYS(                        XK_6,                                                              5)
	TAGKEYS(                        XK_7,                                                              6)
	TAGKEYS(                        XK_8,                                                              7)
	TAGKEYS(                        XK_9,                                                              8)
	{ MODKEY|ShiftMask,             XK_q,                                 quit,                        {0} },
  { MODKEY,                       XK_F1,                                togglecolorfultag,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        cyclelayout,      { .i = +1 } },
	{ ClkLtSymbol,          0,              Button3,        cyclelayout,      { .i = -1 } },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
