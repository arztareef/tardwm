
static const char col_barbg[]       = "#1e1e2e";
static const char col_tagbarbg[]      = "#45475a";
static const char col_normborder[]       = "#444444";
static const char col_normfg[]       = "#cdd6f4";
static const char col_selfg[]       = "#f5e0dc";
static const char col_border[]        = "#86ae7a";
static const char col_borderbar[]   = "#1e1e2e";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_normfg, col_barbg, col_normborder },
	[SchemeSel]  = { col_selfg, col_barbg,  col_border  },
  [SchemeTag]        = { "#585b70",     col_tagbarbg,  "#000000" },
  [SchemeTag1]       = { "#89b4fa",     col_tagbarbg,  "#000000" },
	[SchemeTag2]       = { "#f9e2af",     col_tagbarbg,  "#000000" },
  [SchemeTag3]       = { "#f38ba8",     col_tagbarbg,  "#000000" },
  [SchemeTag4]       = { "#a6e3a1",     col_tagbarbg,  "#000000" },
  [SchemeTag5]       = { "#f5c2e7",     col_tagbarbg,  "#000000" },
	[SchemeTag6]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeTag7]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeTag8]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeTag9]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeLayout]     = { "#f5e0dc",     col_tagbarbg,    "#000000" },
};

