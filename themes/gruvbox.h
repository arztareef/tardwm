static const char col_barbg[]       = "#1d2021";
static const char col_tagbarbg[]      = "#363b3d";
static const char col_normborder[]       = "#444444";
static const char col_normfg[]       = "#ebdbb2";
static const char col_selfg[]       = "#34393b";
static const char col_border[]        = "#86ae7a";
static const char col_borderbar[]   = "#1d2021";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_normfg, col_barbg, col_normborder },
	[SchemeSel]  = { col_selfg, col_barbg,  col_border  },
  [SchemeTag]        = { "#34393b",     col_tagbarbg,  "#000000" },
  [SchemeTag1]       = { "#83a598",     col_tagbarbg,  "#000000" },
	[SchemeTag2]       = { "#fabd2f",     col_tagbarbg,  "#000000" },
  [SchemeTag3]       = { "#fb4934",     col_tagbarbg,  "#000000" },
  [SchemeTag4]       = { "#b8bb26",     col_tagbarbg,  "#000000" },
  [SchemeTag5]       = { "#d3869b",     col_tagbarbg,  "#000000" },
	[SchemeTag6]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeTag7]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeTag8]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeTag9]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeLayout]     = { "#626880",     col_tagbarbg,    "#000000" },
};

