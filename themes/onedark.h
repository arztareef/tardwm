
static const char col_barbg[]       = "#282c33";
static const char col_tagbarbg[]      = "#363b45";
static const char col_normborder[]       = "#444444";
static const char col_normfg[]       = "#bbbbbb";
static const char col_selfg[]       = "#595f61";
static const char col_border[]        = "#86ae7a";
static const char col_borderbar[]   = "#282c33";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_normfg, col_barbg, col_normborder },
	[SchemeSel]  = { col_selfg, col_barbg,  col_border  },
	[SchemeULine]  = { "#cccccc", col_barbg,  "#000000"  },
  [SchemeTag]        = { "#444444",     col_tagbarbg,  "#000000" },
  [SchemeTag1]       = { "#62aeef",     col_tagbarbg,  "#000000" },
	[SchemeTag2]       = { "#e5c07a",     col_tagbarbg,  "#000000" },
  [SchemeTag3]       = { "#d96c70",     col_tagbarbg,  "#000000" },
  [SchemeTag4]       = { "#98c379",     col_tagbarbg,  "#000000" },
  [SchemeTag5]       = { "#c678dd",     col_tagbarbg,  "#000000" },
	[SchemeTag6]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeTag7]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeTag8]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeTag9]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeLayout]     = { "#999999",     col_tagbarbg,    "#000000" },
};

