
static const char col_barbg[]       = "#303446";
static const char col_tagbarbg[]      = "#41465e";
static const char col_normborder[]       = "#444444";
static const char col_normfg[]       = "#bbbbbb";
static const char col_selfg[]       = "#626880";
static const char col_border[]        = "#86ae7a";
static const char col_borderbar[]   = "#303446";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_normfg, col_barbg, col_normborder },
	[SchemeSel]  = { col_selfg, col_barbg,  col_border  },
  [SchemeTag]        = { "#51576d",     col_tagbarbg,  "#000000" },
  [SchemeTag1]       = { "#8caaee",     col_tagbarbg,  "#000000" },
	[SchemeTag2]       = { "#e5c890",     col_tagbarbg,  "#000000" },
  [SchemeTag3]       = { "#e78284",     col_tagbarbg,  "#000000" },
  [SchemeTag4]       = { "#a6d189",     col_tagbarbg,  "#000000" },
  [SchemeTag5]       = { "#f4b8e4",     col_tagbarbg,  "#000000" },
	[SchemeTag6]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeTag7]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeTag8]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeTag9]       = { col_normfg,      col_tagbarbg,  "#000000" },
	[SchemeLayout]     = { "#626880",     col_tagbarbg,    "#000000" },
};

