<style>
  .eprotalt {
    display: inline-block;
    font-size: 30px;
    vertical-align: middle;
  }
</style>

<div class="container">
  <div align="center">
    <h1>Tardwm</h1>
    <p>A ✨fancy fork of dwm 👀 </p>
    <img src="./Screenshots/tardwm.png" alt="tarvim">
  </div>
  <blockquote>
    <p>Note: No code was taken from the siduck's <a href="https://github.com/siduck/chadwm" target="_blank"><u>build</u></a> of
      <a href="httpss://dwm.suckless.org" target="_blank"><u>dwm</u></a>
    </p>
  </blockquote>

  <h2>Navigation</h2>
  <nav>
    <ul>
      <li><a href="#features">Features</a></li>
      <li><a href="#screenshots">Screenshots</a></li>
      <li><a href="#installation">Installation</a></li>
      <li><a href="#Credits">Credits</a></li>
    </ul>
  </nav>

  <div id="features">
    <h2>Features:</h2>
    <ul>
      <li>Lightweight</li>
      <li>+14 Window Layouts</li>
      <li>Pertag support</li>
      <li>Bar Border</li>
      <li>Bar Padding</li>
      <li>Cycle through layouts by clicking the layout button</li>
    </ul>
  </div>

  <div id="screenshots">
    <h2>Screenshots:</h2>
    <a href="https://codeberg.org/arztareef/tardwm/raw/branch/master/Screenshots/tardwm2.png" target="_blank"><img src="./Screenshots/tardwm2.png" alt="screenshot1"></a> <br> 
    <a href="https://codeberg.org/arztareef/tardwm/raw/branch/master/Screenshots/Code.png" target="_blank"><img src="./Screenshots/Code.png" alt="screenshot2"></a><br>
    <a href="https://codeberg.org/arztareef/tardwm/raw/branch/master/Screenshots/garb.png" target="_blank"><img src="./Screenshots/garb.png" alt="screenshot2"></a><br>
    <a href="https://codeberg.org/arztareef/tardwm/raw/branch/master/Screenshots/pokemon.png" target="_blank"><img src="./Screenshots/pokemon.png" alt="screenshot3"></a><br>
    <h3>Catppuccin Frappe theme</h3>
    <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" target="_blank"><img src="./Screenshots/catppuccin.png"
        alt="screenshot4"></a><br>
    <blockquote>
      <p>Link to the <a href="https://codeberg.org/arztareef/sbar" target="_blank"><u>bar</u></a></p>
    </blockquote>
  </div>

  <div id="installation">
    <h2>Installation</h2>
    <p>Paste this code into your terminal <strong>(might require sudo privileges)</strong></p>
    <pre>
      git clone https://codeberg.org/arztareef/tardwm
      cd ./tardwm
      sudo make install
      make clean
    </pre>
  </div>

  <div id="patches">
    <h2>
      Patches
    </h2>
    <p>Here are the list of patches which were used in this build of dwm</p>
    <p>Available on the <a href="https://suckless.org"><u>suckless</u></a> site.</p>
    <ul>
      <li><strong>actualfullscreen</strong></li>
      <li><strong>attachdirection</strong></li>
      <li><strong>autostart</strong></li>
      <li><strong>bar-height</strong></li>
      <li><strong>center</strong></li>
      <li><strong>cyclelayouts</strong></li>
      <li><strong>fixborders</strong></li>
      <li><strong>movestack</strong></li>
      <li><strong>pertag</strong></li>
      <li><strong>status2d-systray</strong></li>
      <li><strong>winicon</strong></li>
    </ul>
    <p>Not available on the suckless site:</p>
    <blockquote>Can be found on the patches folder.</blockquote>
  </div>
  <div id="credits">
    <h2>Credits:</h2>
    <p>a list of contributors who helped and <strong>inspired</strong> me build this fork of dwm:</p>
  </div>
  <ul>
    <li><strong class="eprotalt">HUGE</strong> thanks to <a href="https://www.reddit.com/user/eProTaLT83/">eProTaLT83</a> for spending countless times on helping me build this fork of dwm.</li>
    <li>Thanks to <a href="https://github.com/siduck">siduck</a> for the inspiration of this build :)</li>
  </ul>
</div>
